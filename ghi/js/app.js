function createCard(title, description, pictureUrl, startDate, endDate) {
    const newStart = new Date(startDate).toLocaleDateString();
    const newEnd = new Date(endDate).toLocaleDateString();
    return `
        <div class="col-4">
            <div class="mb-4">
                <div class="card">
                    <div class="shadow">
                        <img src="${pictureUrl}" class="card-img-top">
                        <div class="card-body">
                            <h5 class="card-title">${title}</h5>
                            <p class="card-text">${description}</p>
                        </div>
                    </div>
                        <div class="card-footer">
                            ${newStart} to ${newEnd}
                        </div>
                </div>
            </div>
        </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {

        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const endDate = details.conference.ends;
                    const startDate = details.conference.starts;

                    console.log(details);

                    const html = createCard(title, description, pictureUrl, startDate, endDate);


                    const row = document.querySelector('.row');
                    row.innerHTML += html;
                }
            }


        }
    } catch (e) {
        console.error(e);
    }
});
